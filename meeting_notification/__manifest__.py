{
    'name': "Meeting Notification",
    'summary': "Notifies users for new meetings and confirm or cancel them based on recieved SMS.",
    'description': "Scans new meeting creation in Odoo and sends SMS with a Twilio number with the relative information to the entendees. Also scans the SMS sent to the Twilio number to process it and confirm a meeting or cancel it.",
    'author': "Quentin Juilland",
    'website': "https://57c8-213-55-224-149.ngrok-free.app",
    'category': 'Intelligent Automated Services',
    'version': '17.0.1.0.0',
    'depends': ['base', 'calendar'],
    'data': [  'data.xml',
        ],
    'installable': True,
    'auto_install': False,
}
