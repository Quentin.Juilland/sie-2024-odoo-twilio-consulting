from odoo import models, api
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
from datetime import datetime

class CalendarEvent(models.Model):
    _inherit = 'calendar.event'

    @api.model
    def create(self, vals):
        event = super(CalendarEvent, self).create(vals)
        self.send_notification(event)
        return event

    def send_notification(self, event):
        # Twilio credentials
        twilio_account_sid = "AC92641125c41c894fcdd6a632a75a3809"
        twilio_auth_token = "2a06864637f77ac7fcf7d6aaf19d7db2"
        twilio_phone_number = "+12517259590"

        # Create Twilio client
        client = Client(twilio_account_sid, twilio_auth_token)

        # Log initial event and attendees
        self.env.cr.commit()  # Commit changes to ensure accurate data retrieval
        self.env.cr.execute("SELECT rp.id, rp.name, rp.phone FROM res_partner rp INNER JOIN calendar_event_res_partner_rel rel ON rel.res_partner_id = rp.id WHERE rel.calendar_event_id = %s", (event.id,))

        attendees = self.env.cr.fetchall()
        self.env.cr.rollback()  # Rollback changes to avoid affecting the transaction
        print("Event:", event)
        print("Attendees:", attendees)

        # Send SMS to attendees
        for attendee in attendees:
            if attendee[2]:  # Check if phone number exists
                print("Sending SMS to:", attendee[1], "Phone:", attendee[2])
                try:
                    # Calculate meeting duration
                    start_datetime = event.start
                    end_datetime = event.stop
                    duration = end_datetime - start_datetime
                    duration_hours = duration.seconds // 3600
                    duration_minutes = (duration.seconds % 3600) // 60

                    # Get location
                    location = event.location if event.location else "Online"

                    # Get the videocall location URL
                    videocall_location_url = event.videocall_location or "No videocall location available"

                    message = client.messages.create(
                        body=f"Hello, {attendee[1]}! \nA new meeting (ID: {event.id}) has been scheduled. "
                        f"\nDuration: {duration_hours} hours {duration_minutes} minutes. \nLocation: {location}. "
                        f"\nJoin Meeting: {videocall_location_url}. \n\nTo confirm this meeting, reply with: \n[MeetingID] YES"
                        f"\n\nTo cancel this meeting, reply with \n[MeetingID] NO",
                        from_=twilio_phone_number,
                        to=attendee[2]
                    )
                    print(f"SMS sent to {attendee[1]} at {attendee[2]}: {message.sid}")
                except TwilioRestException as e:
                    print(f"Failed to send SMS to {attendee[1]} at {attendee[2]}: {e}")
            else:
                print("No phone number found for attendee:", attendee[1])
