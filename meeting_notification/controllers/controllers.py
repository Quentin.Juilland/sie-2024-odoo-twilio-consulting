from odoo import http
from odoo.http import request
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException

class MeetingNotificationController(http.Controller):

    @http.route('/twilio/webhook', type='http', auth='public', methods=['POST'], csrf=False)
    def twilio_webhook(self, **kwargs):
        print("Twilio webhook worked !")

        # Get the message body from the incoming request
        message_body = kwargs.get('Body', '')

        print("Full message:", message_body)

        # Parse the incoming message to extract meeting ID and confirmation
        try:
            meeting_id, confirmation = message_body.split()

            # Convert meeting ID to integer
            meeting_id = int(meeting_id)
            print("This is the meeting id :", meeting_id)

            # Update meeting attendance status based on confirmation
            print(confirmation.lower())
            if confirmation.lower() == 'yes':
                print("Confirmation is detected as YES")

                # Get the attendees for the specified meeting ID
                attendees = request.env['calendar.attendee'].sudo().search([('event_id', '=', meeting_id)])
                print("Attendees:", attendees)

                # Iterate over the attendees
                for attendee in attendees:
                    # Get the user information based on the partner_id
                    user = request.env['res.partner'].sudo().browse(attendee.partner_id.id)
                    print("User:", user)

                    # Check if the user has a company name
                    if not user.company_name:
                        # Update the attendee's state to 'accepted'
                        attendee.write({'state': 'accepted'})
                        print(f"User {user.id} state updated to 'accepted'")

                        # Send confirmation message
                        self.send_confirmation(user)
                    else:
                        print(f"User {user.id} has a company name, no need to update state")

            elif confirmation.lower() == 'no':
                print("Confirmation is detected as NO")

                # Get the meeting record
                meeting = request.env['calendar.event'].sudo().search([('id', '=', meeting_id)])

                attendees = request.env['calendar.attendee'].sudo().search([('event_id', '=', meeting_id)])
                print("Attendees:", attendees)

                # Iterate over the attendees
                for attendee in attendees:
                    # Get the user information based on the partner_id
                    user = request.env['res.partner'].sudo().browse(attendee.partner_id.id)
                    print("User:", user)

                    # Check if the user has a company name
                    if not user.company_name:

                        # Delete the meeting
                        meeting.unlink()
                        self.send_cancellation(user)
                        print(f"Meeting {meeting_id} deleted.")

            else:
                # Handle case where confirmation is neither 'Yes' nor 'No'
                pass  # Add appropriate error handling or logging here

        except ValueError:
            # Handle case where message format is incorrect
            pass  # Add appropriate error handling or logging here

        except Exception as e:
            # Handle other exceptions
            pass  # Add appropriate error handling or logging here

        # Return a response to Twilio (optional)
        return "<Response></Response>"

    def send_confirmation(self, user):
        # Twilio credentials
        twilio_account_sid = "AC92641125c41c894fcdd6a632a75a3809"
        twilio_auth_token = "2a06864637f77ac7fcf7d6aaf19d7db2"
        twilio_phone_number = "+12517259590"

        # Create Twilio client
        client = Client(twilio_account_sid, twilio_auth_token)

        try:
            message = client.messages.create(
                body=f"Hello, {user.name}! Your attendance to the meeting has been confirmed.",
                from_=twilio_phone_number,
                to=user.phone
            )
            print(f"Confirmation SMS sent to {user.name} at {user.phone}: {message.sid}")
        except TwilioRestException as e:
            print(f"Failed to send confirmation SMS to {user.name} at {user.phone}: {e}")

    
    def send_cancellation(self, user):
        # Twilio credentials
        twilio_account_sid = "AC92641125c41c894fcdd6a632a75a3809"
        twilio_auth_token = "2a06864637f77ac7fcf7d6aaf19d7db2"
        twilio_phone_number = "+12517259590"

        # Create Twilio client
        client = Client(twilio_account_sid, twilio_auth_token)

        try:
            message = client.messages.create(
                body=f"Hello, {user.name}! Your attendance to the meeting has been cancelled.",
                from_=twilio_phone_number,
                to=user.phone
            )
            print(f"Confirmation SMS sent to {user.name} at {user.phone}: {message.sid}")
        except TwilioRestException as e:
            print(f"Failed to send confirmation SMS to {user.name} at {user.phone}: {e}")

